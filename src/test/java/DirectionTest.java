import com.jora.toyrobotproject.game.model.robot.model.Direction;
import org.junit.Assert;
import org.junit.Test;

/**
 * Testing Direction Class
 */
public class DirectionTest {

    @Test
    public void testRotateToLeft() throws Exception {
        Direction direction = Direction.NORTH;

        direction = direction.leftDirection();
        Assert.assertEquals(direction, Direction.WEST);

        direction = direction.leftDirection();
        Assert.assertEquals(direction, Direction.SOUTH);

        direction = direction.leftDirection();
        Assert.assertEquals(direction, Direction.EAST);

        direction = direction.leftDirection();
        Assert.assertEquals(direction, Direction.NORTH);
    }

    @Test
    public void testRotateToRight() throws Exception {
        Direction direction = Direction.NORTH;

        direction = direction.rightDirection();
        Assert.assertEquals(direction, Direction.EAST);

        direction = direction.rightDirection();
        Assert.assertEquals(direction, Direction.SOUTH);

        direction = direction.rightDirection();
        Assert.assertEquals(direction, Direction.WEST);

        direction = direction.rightDirection();
        Assert.assertEquals(direction, Direction.NORTH);
    }

    @Test
    public void testRotateToRightAndLeft() throws Exception {
        Direction direction = Direction.NORTH;

        direction = direction.rightDirection();
        Assert.assertEquals(direction, Direction.EAST);

        direction = direction.leftDirection();
        Assert.assertEquals(direction, Direction.NORTH);
    }

    @Test
    public void testGetSpecificValue() throws Exception {
        Assert.assertEquals("NORTH", Direction.valueOf(0).name());
        Assert.assertEquals("EAST", Direction.valueOf(1).name());
        Assert.assertEquals("SOUTH", Direction.valueOf(2).name());
        Assert.assertEquals("WEST", Direction.valueOf(3).name());
    }
}
