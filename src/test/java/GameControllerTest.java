import com.jora.toyrobotproject.exceptions.RobotException;
import com.jora.toyrobotproject.game.controller.GameController;
import com.jora.toyrobotproject.game.model.GameEngineModelManager;
import com.jora.toyrobotproject.game.model.robot.model.Direction;
import com.jora.toyrobotproject.game.view.CommandLineViewManager;
import com.jora.toyrobotproject.game.view.View;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.IOException;

import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.*;

public class GameControllerTest {


    /**
     * Testing communication between view and model
     * @throws RobotException
     */
    @Test
    public void testCommunicationMessage() throws RobotException {
        View commandLineViewManager = mock(CommandLineViewManager.class);
        GameEngineModelManager gameModelManager = new GameEngineModelManager();

        GameController gameController = new GameController();
        gameController.setViewManager(commandLineViewManager);
        gameController.setGameModelManager(gameModelManager);
        gameModelManager.addListener(gameController);
        gameController.inputCommand("PLACE 0,0,NORTH");

        verify(commandLineViewManager, times(1)).notifyMessage("done");
        verify(commandLineViewManager, times(0)).close();
    }

    /**
     * Testing communication between view and model
     * @throws RobotException
     */
    @Test
    public void testCommunicationBadCommand() throws RobotException {
        View commandLineViewManager = mock(CommandLineViewManager.class);
        GameEngineModelManager gameModelManager = new GameEngineModelManager();

        GameController gameController = new GameController();
        gameController.setViewManager(commandLineViewManager);
        gameController.setGameModelManager(gameModelManager);
        gameModelManager.addListener(gameController);
        gameController.inputCommand("PLACE");

        verify(commandLineViewManager, times(1)).notifyMessage("Invalid command");
        verify(commandLineViewManager, times(0)).close();
    }

    /**
     * Testing communication between view and model
     * @throws RobotException
     */
    @Test
    public void testCommunicationCloseGame() throws RobotException {
        View commandLineViewManager = mock(CommandLineViewManager.class);
        GameEngineModelManager gameModelManager = new GameEngineModelManager();

        GameController gameController = new GameController();
        gameController.setViewManager(commandLineViewManager);
        gameController.setGameModelManager(gameModelManager);
        gameModelManager.addListener(gameController);
        gameController.inputCommand("EXIT");

        verify(commandLineViewManager, times(1)).notifyMessage("BYE!!");
        verify(commandLineViewManager, times(1)).close();
    }

}

