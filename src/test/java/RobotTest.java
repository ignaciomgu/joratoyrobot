import com.jora.toyrobotproject.exceptions.RobotException;
import com.jora.toyrobotproject.game.model.robot.model.Direction;
import com.jora.toyrobotproject.game.model.robot.model.PositionState;
import com.jora.toyrobotproject.game.model.robot.model.Robot;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test Robot Class
 */
public class RobotTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Testing displacement of the robot
     * @throws RobotException
     */
    @Test
    public void testDisplacement() throws RobotException {

        Robot robot = new Robot.Builder().inThisPositionState(new PositionState(0, 0, Direction.NORTH)).build();

        Assert.assertTrue(robot.move());

        Assert.assertEquals(0, robot.getPosition().getX());
        Assert.assertEquals(1, robot.getPosition().getY());
        Assert.assertEquals(Direction.NORTH, robot.getPosition().getDirection());
    }

    /**
     * Testing displacement and left rotation
     * @throws RobotException
     */
    @Test
    public void testDisplacementAndLeftRotation() throws RobotException {
        Robot robot = new Robot.Builder().inThisPositionState(new PositionState(1, 2, Direction.EAST)).build();

        Assert.assertTrue(robot.move());
        Assert.assertTrue(robot.move());
        Assert.assertTrue(robot.rotateLeft());
        Assert.assertTrue(robot.move());

        Assert.assertEquals(3, robot.getPosition().getX());
        Assert.assertEquals(3, robot.getPosition().getY());
        Assert.assertEquals(Direction.NORTH, robot.getPosition().getDirection());
    }

    /**
     * Testing displacement and right rotation
     * @throws RobotException
     */
    @Test
    public void testDisplacementAndRightRotation() throws RobotException {
        Robot robot = new Robot.Builder().inThisPositionState(new PositionState(2, 1, Direction.WEST)).build();

        Assert.assertTrue(robot.move());
        Assert.assertTrue(robot.move());
        Assert.assertTrue(robot.rotateRight());
        Assert.assertTrue(robot.move());

        Assert.assertEquals(0, robot.getPosition().getX());
        Assert.assertEquals(2, robot.getPosition().getY());
        Assert.assertEquals(Direction.NORTH, robot.getPosition().getDirection());
    }

    /**
     * Testing set position
     * @throws RobotException
     */
    @Test
    public void testSetPosition() throws RobotException {

        Robot robot = new Robot.Builder().inThisPositionState(new PositionState(0, 0, Direction.NORTH)).build();

        Assert.assertTrue(robot.move());

        Assert.assertEquals(0, robot.getPosition().getX());
        Assert.assertEquals(1, robot.getPosition().getY());
        Assert.assertEquals(Direction.NORTH, robot.getPosition().getDirection());

        robot.setPosition(new PositionState(0, 0, Direction.NORTH));
        Assert.assertEquals(Direction.NORTH, robot.getPosition().getDirection());

    }
}