import com.jora.toyrobotproject.game.model.robot.model.Direction;
import com.jora.toyrobotproject.game.model.robot.model.PositionState;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test Position Test Class
 */
public class PositionStateTest {

    @Test
    public void testCalculatePosition() throws Exception {
        PositionState position = new PositionState(0, 0, Direction.EAST);

        PositionState newPosition = position.calculateNewPosition();

        newPosition = newPosition.calculateNewPosition();
        Assert.assertNotEquals(1, newPosition.getX());
        Assert.assertEquals(0, newPosition.getY());
        Assert.assertEquals(Direction.EAST, newPosition.getDirection());

        newPosition.setDirection(Direction.NORTH);
        newPosition = newPosition.calculateNewPosition();
        Assert.assertNotEquals(1, newPosition.getX());
        Assert.assertEquals(1, newPosition.getY());
        Assert.assertNotEquals(Direction.EAST, newPosition.getDirection());

    }
}
