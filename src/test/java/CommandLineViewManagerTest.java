import com.jora.toyrobotproject.exceptions.RobotException;
import com.jora.toyrobotproject.game.controller.GameController;
import com.jora.toyrobotproject.game.view.CommandLineViewManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * Test Command Line View Manager Class integrated with the Controller and Model
 */
public class CommandLineViewManagerTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    public CommandLineViewManagerTest() throws IOException {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Test Good Exit command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandEXITGood() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 7);
        Assert.assertEquals("BYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Bad Exit command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandEXITBad() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("EXITT").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 24);
        Assert.assertEquals("Invalid command\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }


    /**
     * Test Good PLACE command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandPLACEGood() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 0,0,NORTH").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 15);
        Assert.assertEquals("\r\ndone\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Bad PLACE command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandPLACEBad() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 6,6,NORTH").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 20);
        Assert.assertEquals("not allowed\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Bad PLACE command variant
     * @throws RobotException
     */
    @Test
    public void testInsertCommandPLACEBadCommand() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE ").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 24);
        Assert.assertEquals("Invalid command\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Good MOVE command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandMOVEGood() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 0,0,NORTH").thenReturn("MOVE").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 19);
        Assert.assertEquals("done\r\ndone\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Bad MOVE command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandMOVEBad() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 5,5,NORTH").thenReturn("MOVE").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 26);
        Assert.assertEquals("done\r\nnot allowed\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Good LEFT command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandLEFTGood() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 0,0,NORTH").thenReturn("LEFT").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 19);
        Assert.assertEquals("done\r\ndone\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test Good RIGHT command
     * @throws RobotException
     */
    @Test
    public void testInsertCommandRIGHTGood() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 0,0,NORTH").thenReturn("RIGHT").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 19);
        Assert.assertEquals("done\r\ndone\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test provided A
     * @throws RobotException
     */
    @Test
    public void testInsertCommandProvidedTestA() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 0,0,NORTH").thenReturn("MOVE").thenReturn("REPORT").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 30);
        Assert.assertEquals("done\r\ndone\r\n0,1,NORTH\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test provided B
     * @throws RobotException
     */
    @Test
    public void testInsertCommandProvidedTestB() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 0,0,NORTH").thenReturn("LEFT").thenReturn("REPORT").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 29);
        Assert.assertEquals("done\r\ndone\r\n0,0,WEST\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }

    /**
     * Test provided C
     * @throws RobotException
     */
    @Test
    public void testInsertCommandProvidedTestC() throws RobotException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        try {
            when(bufferedReader.readLine()).thenReturn("PLACE 1,2,EAST").thenReturn("MOVE").thenReturn("MOVE").thenReturn("LEFT").thenReturn("MOVE").thenReturn("REPORT").thenReturn("EXIT");
        } catch (IOException e) {
            e.printStackTrace();
        }
        GameController gameController = new GameController();
        CommandLineViewManager commandLineViewManager = new CommandLineViewManager(gameController);
        commandLineViewManager.setReader(bufferedReader);
        gameController.setViewManager(commandLineViewManager);
        commandLineViewManager.init();
        String lastConsoleChars = systemOutRule.getLog().substring(systemOutRule.getLog().length() - 30);
        Assert.assertEquals("done\r\ndone\r\n3,3,NORTH\r\nBYE!!\r\n", lastConsoleChars);
        Assert.assertFalse(commandLineViewManager.isTurnOn());
    }
}
