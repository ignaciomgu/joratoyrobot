import com.jora.toyrobotproject.game.model.game.core.Table;
import com.jora.toyrobotproject.game.model.robot.model.PositionState;
import com.jora.toyrobotproject.game.model.game.core.RectangleTable;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test Rectangle Table Class
 */
public class RectangleTableTest {


    /**
     * Validate isInside() function, means that if you provide a position the function returns you if
     * is inside of the table or not.
     * @throws Exception
     */
    @Test
    public void testIsInsidePosition() throws Exception {

        PositionState mockPosition = mock(PositionState.class);
        when(mockPosition.getX()).thenReturn(8);
        when(mockPosition.getY()).thenReturn(8);

        Table table = new RectangleTable(5, 5);
        Assert.assertFalse(table.isInside(mockPosition));

        when(mockPosition.getX()).thenReturn(1);
        when(mockPosition.getY()).thenReturn(1);

        Assert.assertTrue(table.isInside(mockPosition));

        when(mockPosition.getX()).thenReturn(-1);
        when(mockPosition.getY()).thenReturn(-1);

        Assert.assertFalse(table.isInside(mockPosition));

        table = new RectangleTable(3, 7);

        when(mockPosition.getX()).thenReturn(1);
        when(mockPosition.getY()).thenReturn(1);

        Assert.assertTrue(table.isInside(mockPosition));

        when(mockPosition.getX()).thenReturn(4);
        when(mockPosition.getY()).thenReturn(6);

        Assert.assertFalse(table.isInside(mockPosition));

        when(mockPosition.getX()).thenReturn(4);
        when(mockPosition.getY()).thenReturn(8);

        Assert.assertFalse(table.isInside(mockPosition));
    }

}
