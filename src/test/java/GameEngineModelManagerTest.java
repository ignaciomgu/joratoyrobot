import com.jora.toyrobotproject.game.controller.GameController;
import com.jora.toyrobotproject.game.model.GameEngineModelManager;
import com.jora.toyrobotproject.exceptions.RobotException;
import com.jora.toyrobotproject.game.model.ModelListener;
import com.jora.toyrobotproject.game.model.robot.model.Direction;
import com.jora.toyrobotproject.game.model.robot.model.PositionState;
import com.jora.toyrobotproject.game.model.game.core.RectangleTable;
import com.jora.toyrobotproject.game.model.robot.model.Robot;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test Game Engine Model Manager class
 */
public class GameEngineModelManagerTest {

    final int GAME_TABLE_ROWS = 5;
    final int GAME_TABLE_COLUMNS = 5;
    GameEngineModelManager game;

    @Before
    public void setUp() {
        game = new GameEngineModelManager.Builder().withThisTable(new RectangleTable(GAME_TABLE_COLUMNS, GAME_TABLE_ROWS))
                .withThisRobot(new Robot()).build();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Good placing the robot
     * @throws RobotException
     */
    @Test
    public void testPlaceTheRobotGood() throws RobotException {
        Assert.assertTrue(game.placeTheRobot(new PositionState(0, 0, Direction.NORTH)));
        Assert.assertTrue(game.placeTheRobot(new PositionState(1, 1, Direction.SOUTH)));
        Assert.assertTrue(game.placeTheRobot(new PositionState(0, 1, Direction.WEST)));
        Assert.assertTrue(game.placeTheRobot(new PositionState(2, 2, Direction.NORTH)));
        Assert.assertTrue(game.placeTheRobot(new PositionState(0, 2, Direction.EAST)));
        Assert.assertTrue(game.placeTheRobot(new PositionState(3, 3, Direction.NORTH)));
        Assert.assertTrue(game.placeTheRobot(new PositionState(4, 4, Direction.EAST)));
    }

    /**
     * Bad Placing the robot
     * @throws RobotException
     */
    @Test
    public void testPlaceTheRobotBad() throws RobotException {
        Assert.assertFalse(game.placeTheRobot(new PositionState(6, 6, Direction.WEST)));
        Assert.assertFalse(game.placeTheRobot(new PositionState(-1, 5, Direction.EAST)));
        Assert.assertFalse(game.placeTheRobot(new PositionState(-1, 7, Direction.SOUTH)));
    }

    /**
     * Robot exception placing the robot
     * @throws RobotException
     */
    @Test
    public void testPlaceTheRobotExceptions() throws RobotException {
        thrown.expect(RobotException.class);
        game.placeTheRobot(null);
        thrown.expect(RobotException.class);
        game.placeTheRobot(new PositionState(0, 0, null));
    }

    /**
     * First provided test
     * @throws RobotException
     */
    @Test
    public void testExecuteCommandDisplacementProvidedTestA() throws RobotException {
        game.executeCommand("PLACE 0,0,NORTH");
        Assert.assertEquals("0,0,NORTH", game.executeCommand("REPORT"));
        game.executeCommand("MOVE");
        Assert.assertEquals("0,1,NORTH", game.executeCommand("REPORT"));
    }

    /**
     * Bad command execution
     * @throws RobotException
     */
    @Test
    public void testExecuteCommandBad() throws RobotException {
        thrown.expect(RobotException.class);
        Assert.assertEquals("Invalid command", game.executeCommand("PLACE12NORTH"));
        thrown.expect(RobotException.class);
        Assert.assertEquals("Invalid command", game.executeCommand("LEFFT"));
        thrown.expect(RobotException.class);
        Assert.assertEquals("Invalid command", game.executeCommand("RIGHTT"));
        thrown.expect(RobotException.class);
        Assert.assertEquals("Invalid command", game.executeCommand("RIGHT"));
        Assert.assertEquals("Invalid command", game.executeCommand("MOVE"));
        Assert.assertEquals("Invalid command", game.executeCommand("LEFT"));
        Assert.assertEquals("Invalid command", game.executeCommand("PLACE 6,6,NORTH"));
    }

    /**
     * Second provided test
     * @throws RobotException
     */
    @Test
    public void testExecuteCommandDisplacementProvidedTestB() throws RobotException {
        game.executeCommand("PLACE 0,0,NORTH");
        game.executeCommand("LEFT");
        Assert.assertEquals("0,0,WEST", game.executeCommand("REPORT"));
    }

    /**
     * Third provided test
     * @throws RobotException
     */
    @Test
    public void testExecuteCommandDisplacementProvidedTestC() throws RobotException {
        game.executeCommand("PLACE 1,2,EAST");
        game.executeCommand("MOVE");
        game.executeCommand("MOVE");
        game.executeCommand("LEFT");
        game.executeCommand("MOVE");
        Assert.assertEquals("3,3,NORTH", game.executeCommand("REPORT"));
    }

    @Test
    public void testExecuteCommandDisplacementVariant() throws RobotException {
        game.executeCommand("PLACE 1,2,WEST");
        game.executeCommand("MOVE");
        game.executeCommand("MOVE");
        game.executeCommand("LEFT");
        game.executeCommand("LEFT");
        game.executeCommand("LEFT");
        game.executeCommand("RIGHT");
        Assert.assertEquals("0,2,EAST", game.executeCommand("REPORT"));
    }

    @Test
    public void testAddListener() throws RobotException {
        ModelListener modelListener = new GameController();
        game.addListener(modelListener);
        Assert.assertEquals(modelListener, game.getModelListeners().get(0));
    }

    @Test
    public void testRemoveListener() throws RobotException {
        ModelListener modelListener = new GameController();
        game.addListener(modelListener);
        game.removeListener(modelListener);
        Assert.assertEquals(0, game.getModelListeners().size());
    }


}
