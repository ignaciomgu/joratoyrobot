package com.jora.toyrobotproject.exceptions;

/**
 * This exception represents the exception thrown for the robot
 */
public class RobotException extends Exception {
    public RobotException(String string) {
        super(string);
    }
}
