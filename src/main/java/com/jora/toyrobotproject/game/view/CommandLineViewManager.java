package com.jora.toyrobotproject.game.view;

import com.jora.toyrobotproject.game.controller.GameController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class is in charge of manage the command line view and communicate to the model through the controller the different actions.
 */
public class CommandLineViewManager implements View{

    //Controller
    private GameController gameController;
    //Switch
    private boolean turnOn;
    //Command line reader
    BufferedReader reader;

    /**
     * The constructor assign the controller
     * @param gameController
     */
    public CommandLineViewManager(GameController gameController) {
        this.gameController = gameController;
        this.reader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Starting View
     */
    @Override
    public void init(){
        System.out.println("Hey!... Start your robot and put in some where on the game table!");
        System.out.println("Enter a command:");
        System.out.println("\'PLACE X,Y,NORTH|SOUTH|EAST|WEST\', MOVE, LEFT, RIGHT, REPORT or EXIT");
        this.turnOn = true;
        do{
            try {
                String inputString = reader.readLine();
                this.gameController.inputCommand(inputString.toUpperCase());
            } catch (IOException e) {
                System.out.println("There was a problem trying read the command");
            }
        }while(turnOn);
    }

    @Override
    public void close() {
        this.turnOn = false;
    }

    /**
     * Receiving messages from the model through the controller
     * @param message
     */
    @Override
    public void notifyMessage(String message) {
        System.out.println(message);
    }

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }

    public boolean isTurnOn() {
        return turnOn;
    }

    public void setTurnOn(boolean turnOn) {
        this.turnOn = turnOn;
    }

}
