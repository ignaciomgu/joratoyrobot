package com.jora.toyrobotproject.game.view;

/**
 * This interface is the contract of the different views implementations
 */
public interface View {
    /**
     * Notifying messages from the controller to the view
     * @param message
     */
    public void notifyMessage(String message);

    /**
     * Starting the view component
     */
    public void init();

    /**
     * Shutdown the view
     */
    public void close();
}
