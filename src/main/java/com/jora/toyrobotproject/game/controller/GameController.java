package com.jora.toyrobotproject.game.controller;

import com.jora.toyrobotproject.exceptions.RobotException;
import com.jora.toyrobotproject.game.model.GameEngineModelManager;
import com.jora.toyrobotproject.game.model.ModelListener;
import com.jora.toyrobotproject.game.view.CommandLineViewManager;
import com.jora.toyrobotproject.game.view.View;

/**
 * Mediator manager between the view and the model
 */
public class GameController implements ModelListener {

    //View
    private View commandLineViewManager;
    //Model
    private GameEngineModelManager gameModelManager;

    /**
     * Creating the view and the model
     */
    public GameController() {
        this.commandLineViewManager = new CommandLineViewManager(this);
        this.gameModelManager = new GameEngineModelManager.Builder().withThisListener(this).build();
    }

    /**
     * Processing the game command parameter
     * @param command
     */
    public void inputCommand(String command){
        try {
            String response = this.gameModelManager.executeCommand(command);
            this.commandLineViewManager.notifyMessage(response);
        } catch (RobotException e) {
            this.commandLineViewManager.notifyMessage(e.getMessage());
        }
    }

    /**
     * Initializing the view
     */
    public void initGame(){
        this.commandLineViewManager.init();
    }

    /**
     * Turn off the view of the game
     */
    private void shutdownGame(){
        this.commandLineViewManager.close();
    }

    /**
     * Listening for the shutdown of the game
     */
    @Override
    public void turnOffGame() {
        this.shutdownGame();
    }

    public View getViewManager() {
        return commandLineViewManager;
    }

    public void setViewManager(View commandLineViewManager) {
        this.commandLineViewManager = commandLineViewManager;
    }

    public GameEngineModelManager getGameModelManager() {
        return gameModelManager;
    }

    public void setGameModelManager(GameEngineModelManager gameModelManager) {
        this.gameModelManager = gameModelManager;
    }

}
