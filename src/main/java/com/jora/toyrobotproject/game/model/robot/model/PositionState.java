package com.jora.toyrobotproject.game.model.robot.model;

import com.jora.toyrobotproject.exceptions.RobotException;

/**
 * This class represents the position x,y and direction north, south, etc.
 */
public class PositionState {

    //Cardinal x
    private int x;
    //Cardinal y
    private int y;
    //North, south, etc..
    private Direction direction;

    public PositionState(PositionState position) {
        this.x = position.getX();
        this.y = position.getY();
        this.direction = position.getDirection();
    }

    public PositionState(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    /**
     * Changes the position
     * @param x cardinal
     * @param y cardinal
     */
    public void changePosition(int x, int y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }

    /**
     * Calculating the next position
     * @return new position
     * @throws RobotException
     */
    public PositionState calculateNewPosition() throws RobotException {
        if (this.direction == null)
            throw new RobotException("Invalid direction");

        PositionState positionState = new PositionState(this);
        switch (this.direction) {
            case NORTH:
                positionState.changePosition(0, 1);
                break;
            case EAST:
                positionState.changePosition(1, 0);
                break;
            case SOUTH:
                positionState.changePosition(0, -1);
                break;
            case WEST:
                positionState.changePosition(-1, 0);
                break;
        }
        return positionState;
    }
    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
