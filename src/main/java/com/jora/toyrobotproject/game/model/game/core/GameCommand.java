package com.jora.toyrobotproject.game.model.game.core;

/**
 * This enum contains all different commands.
 */
public enum GameCommand {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    EXIT,
    REPORT
}
