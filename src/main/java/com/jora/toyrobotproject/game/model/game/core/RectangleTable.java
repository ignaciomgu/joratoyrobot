package com.jora.toyrobotproject.game.model.game.core;

import com.jora.toyrobotproject.game.model.robot.model.PositionState;

/**
 * Rectangle Board for the game
 */
public class RectangleTable implements Table {

    //number of rows
    private int numberOfRows;
    //number of columns
    private int numberOfColumns;

    /**
     * Starting the configuration for the table
     * @param rows
     * @param columns
     */
    public RectangleTable(int rows, int columns) {
        this.numberOfRows = rows;
        this.numberOfColumns = columns;
    }

    /**
     * This function validates if this position passed by params is inside of the table
     * @param positionState
     * @return boolean
     */
    @Override
    public boolean isInside(PositionState positionState) {
        return !(positionState.getX()>this.numberOfColumns||positionState.getX()<0||positionState.getY()>this.numberOfRows||positionState.getY()<0);
    }
}
