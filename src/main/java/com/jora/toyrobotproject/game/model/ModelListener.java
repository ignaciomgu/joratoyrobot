package com.jora.toyrobotproject.game.model;

/**
 * This interface represents the contract of the model listeners
 */
public interface ModelListener {
    /**
     * Notifying that the game is finished
     */
    public void turnOffGame();
}
