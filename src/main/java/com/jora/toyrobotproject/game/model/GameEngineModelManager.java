package com.jora.toyrobotproject.game.model;

import com.jora.toyrobotproject.exceptions.RobotException;
import com.jora.toyrobotproject.game.model.game.core.GameCommand;
import com.jora.toyrobotproject.game.model.game.core.RectangleTable;
import com.jora.toyrobotproject.game.model.game.core.Table;
import com.jora.toyrobotproject.game.model.robot.model.Direction;
import com.jora.toyrobotproject.game.model.robot.model.PositionState;
import com.jora.toyrobotproject.game.model.robot.model.Robot;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is in charge of manage the game model
 */
public class GameEngineModelManager {

    //Creating Rectangle Table with 5 rows and 5 columns by default
    private Table gameTable = new RectangleTable(5,5);
    //Creating a robot called SDF1 by default
    private Robot robot = new Robot.Builder().withName("SDF1").build();
    private List<ModelListener> modelListeners = new ArrayList<ModelListener>();

    /**
     * Game Engine builder
     */
    public static class Builder {
        //Creating Rectangle Table with 5 rows and 5 columns by default
        private Table gameTable = new RectangleTable(5,5);
        //Creating a robot called SDF1 by default
        private Robot robot = new Robot.Builder().withName("SDF1").build();
        private List<ModelListener> modelListeners = new ArrayList<ModelListener>();
        public Builder() {

        }
        public Builder withThisTable(Table gameTable){
            this.gameTable = gameTable;
            return this;
        }
        public Builder withThisRobot(Robot robot){
            this.robot = robot;
            return this;
        }
        public Builder withThisListener(ModelListener modelListener){
            this.modelListeners.add(modelListener);
            return this;
        }
        public Builder withTheseListeners(List<ModelListener> modelListeners){
            this.modelListeners = modelListeners;
            return this;
        }
        public GameEngineModelManager build(){
            GameEngineModelManager gameEngineModelManager = new GameEngineModelManager();
            gameEngineModelManager.gameTable = this.gameTable;
            gameEngineModelManager.robot = this.robot;
            gameEngineModelManager.modelListeners = this.modelListeners;
            return gameEngineModelManager;
        }
    }

    /**
     * Places the robot on the game table in the position state passed by parameter
     *
     * @param position
     * @return boolean
     * @throws RobotException
     */
    public boolean placeTheRobot(PositionState position) throws RobotException {
        if (this.gameTable == null)
            throw new RobotException("Invalid game table");
        if (position == null)
            throw new RobotException("Invalid position");
        if (position.getDirection() == null)
            throw new RobotException("Invalid direction");
        if (!this.gameTable.isInside(position))
            return false;
        this.robot.setPosition(position);
        return true;
    }

    /**
     * Validates the command passed by parameter
     *
     * @param inputString command string
     * @return string value of the executed command
     * @throws RobotException
     *
     */
    public String executeCommand(String inputString) throws RobotException {
        String[] args = inputString.split(" ");
        GameCommand command;
        try {
            command = GameCommand.valueOf(args[0]);
        } catch (IllegalArgumentException e) {
            throw new RobotException("Invalid command");
        }
        if (command == GameCommand.PLACE && args.length < 2) {
            throw new RobotException("Invalid command");
        }
        String[] params;
        int x = 0;
        int y = 0;
        Direction commandDirection = null;
        if (command == GameCommand.PLACE) {
            params = args[1].split(",");
            try {
                x = Integer.parseInt(params[0]);
                y = Integer.parseInt(params[1]);
                commandDirection = Direction.valueOf(params[2]);
            } catch (Exception e) {
                throw new RobotException("Invalid command");
            }
        }
        return discriminateCommand(command, new PositionState(x, y, commandDirection));
    }

    /**
     * Execute the command passed by parameter
     * @param command
     * @param positionState
     * @return String
     * @throws RobotException
     */
    private String discriminateCommand(GameCommand command, PositionState positionState) throws RobotException {
        String output;
        switch (command) {
            case PLACE:
                output = placeTheRobot(positionState)?"done":"not allowed";
                break;
            case MOVE:
                PositionState newPosition = robot.getPosition().calculateNewPosition();
                if (!this.gameTable.isInside(newPosition))
                    output = "not allowed";
                else
                    output = this.robot.move(newPosition)?"done":"not allowed";
                break;
            case LEFT:
                output = this.robot.rotateLeft()?"done":"not allowed";
                break;
            case RIGHT:
                output = this.robot.rotateRight()?"done":"not allowed";
                break;
            case REPORT:
                output = report();
                break;
            case EXIT:
                output = shutdown();
                break;
            default:
                throw new RobotException("Invalid command");
        }
        return output;
    }

    /**
     * Returns the position state of the robot is currently
     */
    public String report() {
        if (this.robot.getPosition() == null)
            return null;
        return this.robot.getPosition().getX() + "," + this.robot.getPosition().getY() + "," + this.robot.getPosition().getDirection().toString();
    }

    /**
     * Close game
     * @return cheers
     */
    public String shutdown() {
        this.modelListeners.forEach(ModelListener::turnOffGame);
        return "BYE!!";
    }

    /**
     * Adding new listener to the model
     * @param modelListener
     */
    public void addListener(ModelListener modelListener){
        this.modelListeners.add(modelListener);
    }

    /**
     * Deleting existing listener to the model
     * @param modelListener
     */
    public void removeListener(ModelListener modelListener){
        this.modelListeners.remove(modelListener);
    }
    public List<ModelListener> getModelListeners() {
        return modelListeners;
    }

    public void setModelListeners(List<ModelListener> modelListeners) {
        this.modelListeners = modelListeners;
    }

}
