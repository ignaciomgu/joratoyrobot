package com.jora.toyrobotproject.game.model.robot.model;

import com.jora.toyrobotproject.exceptions.RobotException;

/**
 * Represents the toy robot
 */
public class Robot {

    //Robot name
    private String name;
    //Robot position x,y and direction z
    private PositionState position;

    /**
     * Robot Builder
     */
    public static class Builder {
        private String name;
        private PositionState positionState;
        public Builder() {

        }
        public Robot.Builder withName(String gameTable){
            this.name = name;
            return this;
        }
        public Robot.Builder inThisPositionState(PositionState positionState){
            this.positionState = positionState;
            return this;
        }
        public Robot build(){
            Robot robot = new Robot();
            robot.name = this.name;
            robot.position = this.positionState;
            return robot;
        }
    }

    /**
     * Moves to the next position
     * @return boolean
     * @throws RobotException
     */
    public boolean move() throws RobotException {
        return move(position.calculateNewPosition());
    }

    /**
     * Moves the robot one unit forward in the direction that It is currently
     * @return boolean
     */
    public boolean move(PositionState newPosition) {
        if (newPosition == null)
            return false;
        this.position = newPosition;
        return true;
    }

    public PositionState getPosition() {
        return this.position;
    }

    /**
     * Rotates the robot 90 degrees to the left
     * @return boolean
     */
    public boolean rotateLeft() {
        if (this.position.getDirection() == null)
            return false;

        this.position.setDirection(this.position.getDirection().leftDirection());
        return true;
    }

    /**
     * Rotates the robot 90 degrees to the right
     * @return boolean
     */
    public boolean rotateRight() {
        if (this.position.getDirection() == null)
            return false;
        this.position.setDirection(this.position.getDirection().rightDirection());
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean setPosition(PositionState position) {
        if (position == null)
            return false;
        this.position = position;
        return true;
    }
}
