package com.jora.toyrobotproject.game.model.game.core;

import com.jora.toyrobotproject.game.model.robot.model.PositionState;

/**
 * This interface is the contract of the different kind of tables.
 */
public interface Table {
    /**
     * Validates if the position passed by parameter is inside of the table.
     * @param position
     * @return boolean
     */
    public boolean isInside(PositionState position);
}