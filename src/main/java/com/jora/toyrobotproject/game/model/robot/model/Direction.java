package com.jora.toyrobotproject.game.model.robot.model;


import java.util.HashMap;
import java.util.Map;

/**
 * This enum contains all different directions and It is responsible of calculate the different directions.
 */
public enum Direction {

    //Directions with their ID
    NORTH(0), EAST(1), SOUTH(2), WEST(3);
    //Collection of all directions
    private static Map<Integer, Direction> directionsCollection = new HashMap<Integer, Direction>();
    //ID Direction
    private int directionId;
    //Filling the collection of all directions
    static {
        for (Direction directionEnum : Direction.values()) {
            directionsCollection.put(directionEnum.directionId, directionEnum);
        }
    }

    Direction(int direction) {
        this.directionId = direction;
    }

    /**
     * Get specific direction calculator by id
     * @param directionNum
     * @return
     */
    public static Direction valueOf(int directionNum) {
        return directionsCollection.get(directionNum);
    }

    /**
     * Returns the direction on the left
     */
    public Direction leftDirection() {
        return rotate(-1);
    }

    /**
     * Returns the direction on the right
     */
    public Direction rightDirection() {
        return rotate(1);
    }

    /**
     * Calculating the next direction by rotation
     * @param indexOfRotation
     * @return direction
     */
    private Direction rotate(int indexOfRotation) {

        int directionID = (this.directionId + indexOfRotation) < 0 ?
                directionsCollection.size() - 1 :
                (this.directionId + indexOfRotation) % directionsCollection.size();

        return Direction.valueOf(directionID);
    }

}
