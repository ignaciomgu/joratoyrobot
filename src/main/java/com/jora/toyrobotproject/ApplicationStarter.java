package com.jora.toyrobotproject;

import com.jora.toyrobotproject.game.controller.GameController;

/**
 * This is the Main of the project. This class is in charge of start the application
 */
public class ApplicationStarter {

    public static void main(String[] args) {
        /**
         * Creating the controller
         */
        GameController gameController = new GameController();
        /**
         * Starting the application
         */
        gameController.initGame();
    }
}